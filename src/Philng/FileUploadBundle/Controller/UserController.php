<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 10/7/14
 * Time: 9:14 PM
 */
namespace Philng\FileUploadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Philng\FileUploadBundle\Entity\SimpleUsers;

class UserController extends Controller{

    public function loginAction(){
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'PhilngFileUploadBundle:User:login.html.twig',
            array(
                // last username entered by the user
                'error'         => $error
            )
        );
    }

    public function logoutAction(){
        $this->get('security.context')->setToken(null);
        $this->get('request')->getSession()->invalidate();

        return $this->redirect($this->generateUrl('homepage'));

    }

    public function registerAction(){
        return $this->render('PhilngFileUploadBundle:User:register.html.twig', array());
    }

    public function registerDoAction(Request $request){
        $newUser = new SimpleUsers();
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($newUser);


        $newUser->setUsername($request->request->get('username'));
        $newUser->setEmail($request->request->get('email'));
        $newUser->setSalt(md5(uniqid()));

        $newUser->setPassword(
            $encoder->encodePassword($request->request->get('password'), $newUser->getSalt())
        );


        $validator = $this->get('validator');
        $errors = $validator->validate($newUser);

        if (count($errors) > 0) {
            return $this->render('PhilngFileUploadBundle:User:register.html.twig',
                array("errors" => (String) $errors));
        } else {
            $manager = $this->getDoctrine()->getManager();

            $manager->persist($newUser);
            $manager->flush();

            return $this->redirect($this->generateUrl('login'));
        }

    }

}