<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 10/9/14
 * Time: 9:34 PM
 */

namespace Philng\FileUploadBundle\Controller;

use Philng\FileUploadBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileManagementController extends Controller{

    public function myFilesAction(){
        $manager = $this->getDoctrine()->getManager()->getRepository('PhilngFileUploadBundle:File');
        $usr= $this->get('security.context')->getToken()->getUser();

        $files = $manager->findBy(array('user' => $usr));

        $fileArr = array();


        return $this->render("PhilngFileUploadBundle:FileManagement:myFiles.html.twig", array('files' => $files));
    }

    public function uploadAction(Request $request){

        $fileLocation = $this->get('kernel')->getRootDir() . "/.." . $this->container->getParameter("fileUploadDirectory");

        $file = $request->files->get("file");

        $fileEntity = new File();
        $fileEntity->setNewHashID($file->getClientOriginalName());

        $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        //Save the file
        $file->move($fileLocation, $fileEntity->getHashid() . "." . $fileExtension);

        $fileEntity->setUser($this->get('security.context')->getToken()->getUser());
        $fileEntity->setNumdownloads(0);
        $fileEntity->setFilename($fileEntity->getHashid() . "." . $fileExtension);
        $fileEntity->setName($file->getClientOriginalName());

        //Save to DB
        $manager = $this->getDoctrine()->getManager();

        $manager->persist($fileEntity);
        $manager->flush();

        return $this->redirect($this->generateUrl('myfiles'));

    }

    public function deleteAction($id){
        $entityManager = $this->getDoctrine()->getManager();
        $manager = $this->getDoctrine()->getManager()->getRepository('PhilngFileUploadBundle:File');
        $usr= $this->get('security.context')->getToken()->getUser();

        $file = $manager->findBy(array('id' => $id, 'user' => $usr));

        if(count($file) != 1){
            return $this->redirect($this->generateUrl('myfiles'));
        } else {
            $entityManager->remove($file[0]);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('myfiles'));
        }
    }

    public function downloadAction($hashid){
        $manager = $this->getDoctrine()->getManager()->getRepository('PhilngFileUploadBundle:File');

        $file = $manager->findOneBy(array('hashid' => $hashid));


        if(!$file){
            throw $this->createNotFoundException('The file does not exist');
        }
        $file->setNumdownloads( $file->getNumdownloads() + 1 );

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $fileExtension = pathinfo($file->getName(), PATHINFO_EXTENSION);

        $imageExt = array("jpg", "png", "bmp");

        if(!in_array(strtolower($fileExtension), $imageExt)) {
            $headers = array(
                'Content-Type' => "application/force-download",
                'Content-Disposition' => 'attachment; filename="' . $file->getName() . '"'
            );
        } else {
            $headers = array(
                'Content-Type' => "image/jpeg"
            );
        }
        $fileLocation = $this->get('kernel')->getRootDir() . "/.." . $this->container->getParameter("fileUploadDirectory") . $file->getFileName();

        return new Response(file_get_contents($fileLocation), 200, $headers);

    }


}