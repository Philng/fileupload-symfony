<?php

namespace Philng\FileUploadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * File
 */
class File
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $hashid;

    /**
     * @var integer
     */
    private $numdownloads;

    /**
     * @var user
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set hashid
     *
     * @param string $hashid
     * @return File
     */
    public function setHashid($hashid)
    {
        $this->hashid = $hashid;

        return $this;
    }

    /**
     * Get hashid
     *
     * @return string 
     */
    public function getHashid()
    {
        return $this->hashid;
    }

    /**
     * Set numdownloads
     *
     * @param integer $numdownloads
     * @return File
     */
    public function setNumdownloads($numdownloads)
    {
        $this->numdownloads = $numdownloads;

        return $this;
    }

    /**
     * Get numdownloads
     *
     * @return integer 
     */
    public function getNumdownloads()
    {
        return $this->numdownloads;
    }

    /**
     * Set user
     *
     * @param \Philng\FileUploadBundle\Entity\SimpleUsers $user
     * @return File
     */
    public function setUser(\Philng\FileUploadBundle\Entity\SimpleUsers $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Philng\FileUploadBundle\Entity\SimpleUsers 
     */
    public function getUser()
    {
        return $this->user;
    }


    public function setNewHashID($salt = ""){
        $newHash = md5(microtime() . $salt);
        $this->setHashid($newHash);

        return $newHash;
    }
}
